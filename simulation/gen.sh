rm -rf gen/go
mkdir -p gen/go

protoc -I . \
  --go_out ./gen/go --plugin=protoc-gen-go=/Users/tuonglv/work/go/bin/protoc-gen-go \
  --go-grpc_out ./gen/go --plugin=protoc-gen-go-grpc=/Users/tuonglv/work/go/bin/protoc-gen-go-grpc \
  --grpc-gateway_opt logtostderr=true \
  --grpc-gateway_opt generate_unbound_methods=true \
  --grpc-gateway_out ./gen/go --plugin=protoc-gen-grpc-gateway=/Users/tuonglv/work/go/bin/protoc-gen-grpc-gateway \
  simulation.proto

protoc -I . \
  --go_out ./gen/go --plugin=protoc-gen-go=/Users/tuonglv/work/go/bin/protoc-gen-go \
  --go-grpc_out ./gen/go --plugin=protoc-gen-go-grpc=/Users/tuonglv/work/go/bin/protoc-gen-go-grpc \
  --grpc-gateway_opt logtostderr=true \
  --grpc-gateway_opt generate_unbound_methods=true \
  --grpc-gateway_out ./gen/go --plugin=protoc-gen-grpc-gateway=/Users/tuonglv/work/go/bin/protoc-gen-grpc-gateway \
  google/api/annotations.proto

cp -r gen/go/bitbucket.org/tuongly/internal-apis/* .